#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 18:11:11 2020

@author: krishna
"""

import glob
import os
import numpy as np
import soundfile as sf
from shutil import copyfile


def extract_audio(video_filepath):
    #dump_audio_path = os.path.join(dump_dir,video_filepath.split('/')[-1][:-4]+'.wav')
    extract_audio = 'ffmpeg -i '+video_filepath+' -f wav '+video_filepath[:-4]+'.wav'
    try:
        os.system(extract_audio)
        print('Extracted audio succesfully for {} '.format(video_filepath))
        dump_audio_path = video_filepath[:-4]+'.wav'
        return dump_audio_path
    except:
        print('Not able to extract due to some errors')
        


def downsample(audio_filepath):
    downsample = 'sox  '+audio_filepath+' -r 16k -c 1  '+audio_filepath[:-4]+'_16k.wav'
    try:
        os.system(downsample)
        print('Successfully downsampled {}'.format(audio_filepath))
        return audio_filepath[:-4]+'_16k.wav'
    except:
        print('Not able to downsample due to some errors')
        

def chunk_big_files(video_filepath,create_video_dump_dir,max_duration=1200):
    segment_folder = os.path.join(create_video_dump_dir,'segments')
    if not os.path.exists(segment_folder):
        os.makedirs(segment_folder)
    
    #'ffmpeg -i input.mp4 -c copy -map 0 -segment_time 00:20:00 -f segment -reset_timestamps 1 output%03d.mp4'
    segment_audio='ffmpeg -i '+video_filepath+' -c copy -map 0 -segment_time 00:20:00 -f segment -reset_timestamps 1 '+segment_folder+'/segments_%08d'+video_filepath[-4:]
    try:
        os.system(segment_audio)
    except:
        print('Error during chunking big file')
    return segment_folder
