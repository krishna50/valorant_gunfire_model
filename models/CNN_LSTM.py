#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 19:46:03 2020

@author: Krishna
"""

import torch
import torch.nn as nn
import torch.nn.functional as F
from modules_funs import ConvBlock, LinearAttentionBlock, ProjectorBlock
from initialize import *



class Conv_LSTM(nn.Module):
    def __init__(self, num_classes=2, attention=True, normalize_attn=True, init='xavierUniform'):
        super(Conv_LSTM, self).__init__()
        self.attention = attention        
        self.conv_block1 = ConvBlock(513, 256, 1)
        self.conv_block2 = ConvBlock(256, 256, 1)
        self.conv_block3 = ConvBlock(256, 256, 1)
        self.conv_block4 = ConvBlock(256, 256, 2)
        self.conv_block5 = ConvBlock(256, 128, 2)
        self.lstm = nn.LSTM(input_size=128, hidden_size=256,num_layers=1,bidirectional=True,dropout=0.3,batch_first=True)
        self.fc = nn.Linear(in_features=1024, out_features=512, bias=True)
        self.classify = nn.Linear(in_features=512, out_features=num_classes, bias=True)
        
    def forward(self,inputs):
        
        x = self.conv_block1(inputs)
        x = self.conv_block2(x)
        x = self.conv_block3(x)
        x = F.max_pool1d(self.conv_block4(x), kernel_size=2, stride=2, padding=0)
        x = F.max_pool1d(self.conv_block5(x), kernel_size=2, stride=2, padding=0)
        x = x.permute(0,2,1)
        lstm_out,_ = self.lstm(x)
        x = lstm_out.permute(0,2,1)
        mean = torch.mean(x,2)
        var = torch.var(x,2)
        stat_pool = torch.cat((mean,var),1)
        # classification layer
        #x = self.sigmoid(self.classify(x))
        x = self.fc(stat_pool)
        x = self.classify(x)
        return x
    

