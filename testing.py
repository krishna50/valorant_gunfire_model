#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 12:01:48 2020

@author: krishna
"""

import torch
import numpy as np
import datetime
import json
import os


from torch.utils.data import DataLoader   
from audio_classifier_valaront.SpeechDataGenerator import SpeechDataGenerator
import torch.nn as nn
import os
import sox
import numpy as np
from torch import optim
import librosa
from audio_classifier_valaront.models.CNN_LSTM import Conv_LSTM
from sklearn.metrics import accuracy_score
import glob
import torch.nn.functional as F
from audio_classifier_valaront.helpers import infer_data
torch.multiprocessing.set_sharing_strategy('file_system')
#### Dataset info
spec_len=300
class_ids = {0:'filler',1:'gunfire'}


device='cpu'
model = Conv_LSTM(num_classes=2)
model.load_state_dict(torch.load('/home/krishna/Krishna/Sizzle_projects/sizzle_full_pipeline/sizzle_pipeline/audio_classifier_valaront/model_checkpoint_cnn_lstm/best_check_point_24_0.9203354297693921',map_location='cpu')['model'])
optimizer = optim.Adam(model.parameters(), lr=0.001, weight_decay=0.0, betas=(0.9, 0.98), eps=1e-9)
loss = nn.CrossEntropyLoss()



def inference_audio_classifier_valorant(audio_filepath):
    model.eval()
    with torch.no_grad():
        chunks =infer_data(audio_filepath)
        features = torch.from_numpy(np.asarray([torch_tensor.numpy() for torch_tensor in chunks]))
        features=features.to(device)
        preds = model(features)
        #total_loss = loss(preds, labels.squeeze())
        predictions = np.argmax(preds.detach().cpu().numpy(),axis=1)
        full_preds=[]
        for predi in predictions:
            pred_class = class_ids[predi]
            full_preds.append(pred_class)
          #  print(pred_class)
        print('prediction for  {} is {}'.format(audio_filepath,full_preds))
        
    return full_preds


def create_srt(start_dur,end_dur,label,index):
    start_chunk = str(datetime.timedelta(seconds=start_dur))
    hr = start_chunk.split(':')[0]
    mins= start_chunk.split(':')[1]
    sec  = start_chunk.split(':')[2]
    if len(sec.split('.'))==2:
        sec_format = sec.split('.')[0]+','+sec.split('.')[1][:3]
    else:
        sec_format = sec+','+'000'
    join_start = hr+':'+mins+':'+sec_format
    
   
    #####
    end_chunk = str(datetime.timedelta(seconds=end_dur))
    hr = end_chunk.split(':')[0]
    mins= end_chunk.split(':')[1]
    sec  = end_chunk.split(':')[2]
    if len(sec.split('.'))==2:
        sec_format = sec.split('.')[0]+','+sec.split('.')[1][:3]
    else:
        sec_format = sec+','+'000'
    
    join_end  = hr+':'+mins+':'+sec_format
    row_1=index
    row_2 =join_start+' --> '+join_end
    row_3 = label
    return row_1,row_2,row_3

        

############
import sox
video_filepath='/media/newhd/sizzle_dump/VAL_643360357/segments/segments_00000001.mp4'
audio_filepath ='/media/newhd/sizzle_dump/VAL_643360357/segments/segments_00000001.wav'
dur = sox.file_info.duration(audio_filepath)
full_list=[]
for i in range(0,int(dur)-3,3):
    start_time = i
    end_time = i+3.0
    tfm = sox.Transformer()
    tfm.trim(start_time,	end_time)
    tfm.build(audio_filepath, 'temp.wav')
    cut_seg_path = 'temp.wav'
    prediction = inference_audio_classifier_valorant(cut_seg_path)
    list_data =[start_time,end_time,prediction[0]]
    full_list.append(list_data)
    os.remove(cut_seg_path)




all_videos = sorted(glob.glob('/media/newhd/sizzle_dump/VAL_643360357/segments/*.mp4'))
store_path ='/media/newhd/sizzle_dump/VAL_643360357/gunfire_clips'
global_count=0
for video_filepath  in all_videos:      
    audio_filepath = video_filepath[:-4]+'.wav'
    dur = sox.file_info.duration(audio_filepath)
    full_list=[]
    for i in range(0,int(dur)-3,3):
        start_time = i
        end_time = i+3.0
        tfm = sox.Transformer()
        tfm.trim(start_time,	end_time)
        tfm.build(audio_filepath, 'temp.wav')
        cut_seg_path = 'temp.wav'
        prediction = inference_audio_classifier_valorant(cut_seg_path)
        list_data =[start_time,end_time,prediction[0]]
        full_list.append(list_data)
        os.remove(cut_seg_path)
        
        
        
    
    count=1
    fid = open(video_filepath[:-4]+'.srt','w')
    for item in full_list:
        start_time = item[0]
        end_time = item[1]
        label = item[2]
        hh_mm_ss_start_time = str(datetime.timedelta(seconds=start_time))
        hh_mm_ss_end_time = str(datetime.timedelta(seconds=end_time))
        row_1,row_2,row_3 =create_srt(start_time,end_time,label,count)
        count+=1
        fid.write(str(row_1)+'\n')
        fid.write(str(row_2)+'\n')
        fid.write(str(row_3)+'\n')
        fid.write('\n')
        if label=='gunfire':
            segment= store_path+'/'+'segment-%004d.mp4' % (global_count,)
            cut_video = 'ffmpeg -i '+video_filepath+' -ss '+hh_mm_ss_start_time+' -to '+hh_mm_ss_end_time+' -c:v copy -c:a copy '+segment
            os.system(cut_video)
            global_count+=1
    fid.close()


'''
count=1
fid = open(video_filepath[:-4]+'.srt','w')
for item in full_list:
    start_time = item[0]
    end_time = item[1]
    label = item[2]
    row_1,row_2,row_3 =create_srt(start_time,end_time,label,count)
    count+=1
    fid.write(str(row_1)+'\n')
    fid.write(str(row_2)+'\n')
    fid.write(str(row_3)+'\n')
    fid.write('\n')
fid.close()
'''
        



########################################

################################################################################
json_file = '/home/krishna/Krishna/kaldi/egs/speech_vs_non_speech/v1/result.json'
video_file = '/media/newhd/sizzle_dump/chocoTaco_Goes_on_a_Killing_Spree_ft._chun_and_Boom_-_COD_Warzone_Gameplay-P9xJCtMsUVI/segments/segments_00000000.mp4'
audio_filepath = '/media/newhd/sizzle_dump/chocoTaco_Goes_on_a_Killing_Spree_ft._chun_and_Boom_-_COD_Warzone_Gameplay-P9xJCtMsUVI/segments/segments_00000000.wav'
f = open(json_file) 
data = json.load(f) 
#fid = open(video_file[:-4]+'_snp_kaldi.srt','w')
full_list=[]
count=1
for item in data['non-speech']:
    start_time = item['start_time']
    end_time = item['end_time']
    label = 'non-speech'
    tfm = sox.Transformer()
    tfm.trim(start_time,	end_time)
    tfm.build(audio_filepath, 'temp.wav')
    cut_seg_path = 'temp.wav'
    prediction = inference_audio_classifier_valorant(cut_seg_path)
    
    list_data =[start_time,end_time,'_'.join(prediction)]
    full_list.append(list_data)
    os.remove(cut_seg_path)

count=1
fid = open(video_file[:-4]+'_kaldi_ac.srt','w')
for item in full_list:
    start_time = item[0]
    end_time = item[1]
    label = item[2]
    row_1,row_2,row_3 =create_srt(start_time,end_time,label,count)
    count+=1
    fid.write(str(row_1)+'\n')
    fid.write(str(row_2)+'\n')
    fid.write(str(row_3)+'\n')
    fid.write('\n')
fid.close()













