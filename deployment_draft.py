#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 11:28:43 2020

@author: ankita
"""

import sox
import os
import json 
from create_srt import create_srt
import argparse
import torch
import numpy as np
from models.CNN_LSTM import Conv_LSTM
import glob
from shutil import rmtree
from helpers import infer_data
from shutil import copyfile
import sys
from utils.utility import extract_audio
from utils.utility import chunk_big_files
torch.multiprocessing.set_sharing_strategy('file_system')

#from create_srt import create_srt

class Inference(object):
    def __init__(self,model_path,use_gpu):
        super(Inference, self).__init__()
        self.model_path = model_path
        self.use_gpu = use_gpu
        self.spec_len=300
        self.class_ids = {0:'filler',1:'gunfire'}
        
    def load_model(self):
        self.model = Conv_LSTM(num_classes=2)
        if self.use_gpu:
            self.model.load_state_dict(torch.load(self.model_path)['model'])
            print('Model loaded succefully on GPU')
        else:
            self.model.load_state_dict(torch.load(self.model_path,map_location='cpu')['model'])
            print('Model loaded succefully on CPU')
            
    def infer(self,audio_filepath):

        device = torch.device("cuda" if self.use_gpu else "cpu")
        self.model.eval()
        with torch.no_grad():
            chunks =infer_data(audio_filepath)
            features = torch.from_numpy(np.asarray([torch_tensor.numpy() for torch_tensor in chunks])).float()
            features=features.to(device)
            preds = self.model(features)
        #total_loss = loss(preds, labels.squeeze())
            predictions = np.argmax(preds.detach().cpu().numpy(),axis=1)
            full_preds=[]
            for predi in predictions:
                pred_class = self.class_ids[predi]
                full_preds.append(pred_class)
          #  print(pred_class)
            print('prediction is {}'.format(full_preds))
        
        return full_preds
        
    
    

def data_prep(video_filepath,dump_dir,max_duration=1200):
    
    if not os.path.exists(dump_dir):
        os.makedirs(dump_dir)
    create_video_dump_dir = os.path.join(dump_dir,'_'.join(video_filepath.split('/')[-1][:-4].split(' ')))
    new_video_loc = os.path.join(create_video_dump_dir,'_'.join(video_filepath.split('/')[-1].split(' ')))
    
    try:
        os.makedirs(create_video_dump_dir)
    except:
        print('Folder exist! Please check the video if it is already processed :D')
        print('Exciting the code!!! Delete the folder and rerun the code')
        sys.exit()
        
    copyfile(video_filepath,new_video_loc)
    
    print('Chunking the audio into 20 minutes segments')
    segment_folder=chunk_big_files(new_video_loc,create_video_dump_dir,max_duration) 
    return segment_folder,create_video_dump_dir





def run_detector(inference,segment_folder,video_folder,max_duration=1200):
    video_segments = sorted(glob.glob(segment_folder+'/*.mp4'))
    segment_id=0
    create_json = []
    for video_filepath in video_segments:
        audio_filepath = extract_audio(video_filepath)
        dur = sox.file_info.duration(audio_filepath)
        ### Downsample
        #audio_filepath_16k = downsample(audio_filepath)
        for i in range(0,int(dur)-3,3):
            start_time = i
            end_time = i+3.0
            tfm = sox.Transformer()
            tfm.trim(start_time,	end_time)
            tfm.build(audio_filepath, 'temp.wav')
            cut_seg_path = 'temp.wav'
            preds = inference.infer(cut_seg_path)
            create_dict={}
            create_dict['start_time'] = start_time+float(segment_id*max_duration)
            create_dict['end_time'] = end_time+float(segment_id*max_duration)
            create_dict['label'] = preds[0]
            
            create_json.append(create_dict)
            os.remove(cut_seg_path)
        segment_id+=1 
        
    return create_json


def create_subtitles(json_data,srt_filepath):
    fid = open(srt_filepath,'w')
    for i in range(len(json_data)):
        item =json_data[i]
        start_time = item['start_time']
        end_time = item['end_time']
        label = item['label']
        row_1,row_2,row_3 =create_srt(start_time,end_time,label,i)
        fid.write(str(row_1)+'\n')
        fid.write(str(row_2)+'\n')
        fid.write(str(row_3)+'\n')
        fid.write('\n')
    fid.close()

def main(args):
    
    if args.gameplay == 'cod':
        model_path = './model_checkpoint_cnn_lstm_cod/best_check_point_99_0.895'
    else:
        model_path ='./model_checkpoint_cnn_lstm/best_check_point_24_0.92'
    
    segment_folder, video_folder = data_prep(args.video_filepath,args.dump_dir,max_duration=1200)
    inference = Inference(model_path,args.use_gpu)
    inference.load_model()
    json_results = run_detector(inference,segment_folder,video_folder,max_duration=1200)
    rmtree(video_folder)
    if args.save_results:
        save_path = args.video_filepath[:-4]+'.json'
        with open(save_path,'w') as fobj:
            json.dump(json_results,fobj,indent=2) 
            print('Results are saved at {}'.format(save_path))
        if args.generate_subtitles:
            create_subtitles(json_results,args.video_filepath[:-4]+'.srt')
            print('Subtitles are created at {}'.format(args.video_filepath[:-4]+'.srt'))
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--video_filepath',type=str,default='/home/krishna/Desktop/Valorant_test/VALORANT_test_001.mp4')
    parser.add_argument('--gameplay', type=str, default='cod')
    parser.add_argument('--dump_dir', type=str, default='./dump/')
    parser.add_argument('--use_gpu', type=bool, default=False)
    parser.add_argument('--save_results', type=bool, default=True)
    parser.add_argument('--generate_subtitles', type=bool, default=True)
    
    args = parser.parse_args()
    print(args)
    main(args)



