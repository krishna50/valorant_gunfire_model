#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 12:01:48 2020

@author: krishna
"""

from torch.utils.data import DataLoader   
from audio_classifier_valaront.SpeechDataGenerator import SpeechDataGenerator
import torch.nn as nn
import os
import sox
import torch
import numpy as np
from torch import optim
import librosa
from audio_classifier_valaront.models.CNN_LSTM import Conv_LSTM
from sklearn.metrics import accuracy_score
import glob
import torch.nn.functional as F
from audio_classifier_valaront.helpers import infer_data
torch.multiprocessing.set_sharing_strategy('file_system')


class Inference(object):
    def __init__(self,args):
        super(Inference, self).__init__()
        self.model_path = args.model_path
        self.use_gpu = args.use_gpu
        
        
    def load_model(self,model):
        
        if self.use_gpu:
            model.load_state_dict(torch.load(self.model_path)['model'])
        else:
            model.load_state_dict(torch.load(self.model_path,map_location='cpu')['model'])
        return model
    
    def infer(self,audio_filepath):
        model = Conv_LSTM(num_classes=2)
        model = self.load_model(model)
        model.eval()
        with torch.no_grad():



#### Dataset info
spec_len=300
class_ids = {0:'filler',1:'gunfire'}


device='cpu'
model = Conv_LSTM(num_classes=2)
model.load_state_dict(torch.load('/home/krishna/Krishna/Sizzle_projects/sizzle_full_pipeline/sizzle_pipeline/audio_classifier_valaront/model_checkpoint_cnn_lstm/best_check_point_24_0.9203354297693921',map_location='cpu')['model'])
optimizer = optim.Adam(model.parameters(), lr=0.001, weight_decay=0.0, betas=(0.9, 0.98), eps=1e-9)
loss = nn.CrossEntropyLoss()



def inference_audio_classifier_valorant(audio_filepath):
    model.eval()
    with torch.no_grad():
        chunks =infer_data(audio_filepath)
        features = torch.from_numpy(np.asarray([torch_tensor.numpy() for torch_tensor in chunks]))
        features=features.to(device)
        preds = model(features)
        #total_loss = loss(preds, labels.squeeze())
        predictions = np.argmax(preds.detach().cpu().numpy(),axis=1)
        full_preds=[]
        for predi in predictions:
            pred_class = class_ids[predi]
            full_preds.append(pred_class)
          #  print(pred_class)
        print('prediction for  {} is {}'.format(audio_filepath,full_preds))
        
    return full_preds




    
        
        
        
        
        
        
        
