#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 28 20:38:32 2019

@author: krishna
"""

import datetime
def create_srt(start_dur,end_dur,label,index):
    start_chunk = str(datetime.timedelta(seconds=start_dur))
    hr = start_chunk.split(':')[0]
    mins= start_chunk.split(':')[1]
    sec  = start_chunk.split(':')[2]
    if len(sec.split('.'))==2:
        sec_format = sec.split('.')[0]+','+sec.split('.')[1][:3]
    else:
        sec_format = sec+','+'000'
    join_start = hr+':'+mins+':'+sec_format
   
   
    #####
    end_chunk = str(datetime.timedelta(seconds=end_dur))
    hr = end_chunk.split(':')[0]
    mins= end_chunk.split(':')[1]
    sec  = end_chunk.split(':')[2]
    if len(sec.split('.'))==2:
        sec_format = sec.split('.')[0]+','+sec.split('.')[1][:3]
    else:
        sec_format = sec+','+'000'
    
    join_end  = hr+':'+mins+':'+sec_format
    row_1=index
    row_2 =join_start+' --> '+join_end
    row_3 = label
    return row_1,row_2,row_3
    
    
