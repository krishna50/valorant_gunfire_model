#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 23:01:09 2020

@author: krishna
"""

import librosa
import numpy as np
import sox
import torch


spec_len=300
def lin_spectogram_from_wav(wav, hop_length, win_length, n_fft=1024):
    linear = librosa.stft(wav, n_fft=n_fft, win_length=win_length, hop_length=hop_length) # linear spectrogram
    return linear.T



def infer_data(audio_filepath):
    audio_data,fs = librosa.load(audio_filepath,sr=44100)

    if len(audio_data)<=132300:
        ret=[]
        dummy = np.zeros((1,132300-len(audio_data)))
        new_audio_data = np.concatenate((audio_data,dummy[0]))
        #final_ret=[]
        #final_ret.append(new_audio_data)
        linear_spect = lin_spectogram_from_wav(new_audio_data, hop_length=441, win_length=882, n_fft=1024)
        mag, _ = librosa.magphase(linear_spect)  # magnitude
        mag_T = mag.T
        spec_mag = mag_T[:, :spec_len]
        mu = np.mean(spec_mag, 0, keepdims=True)
        std = np.std(spec_mag, 0, keepdims=True)
        ret_spec= (spec_mag - mu) / (std + 1e-5)
        ret.append(ret_spec)
    else:
        file_dur=sox.file_info.duration(audio_filepath)
        left_seg=(file_dur-int(file_dur/3.0)*3.0)
        if left_seg>0:
            next_point=(int(file_dur/3.0)*3.0+3.0)*44100
            dummy = np.zeros((1,int(next_point)-len(audio_data)))
            new_audio_data = np.concatenate((audio_data,dummy[0]))
        else:
            new_audio_data = audio_data
        
        linear_spect = lin_spectogram_from_wav(new_audio_data, hop_length=441, win_length=882, n_fft=1024)
        mag, _ = librosa.magphase(linear_spect)  # magnitude
        mag_T = mag.T
        no_chunks = int(mag_T.shape[1]/spec_len)
        ret=[]
        for index in range(no_chunks):
            start_index_sec = index*spec_len
            end_index_sec = index*spec_len+spec_len
            #print(start_index_sec,end_index_sec)
            spec_mag = mag_T[:,start_index_sec:end_index_sec]
            mu = np.mean(spec_mag, 0, keepdims=True)
            std = np.std(spec_mag, 0, keepdims=True)
            ret_spec = (spec_mag - mu) / (std + 1e-5)
            ret.append(ret_spec)
    
    return torch.from_numpy(np.ascontiguousarray(ret))
