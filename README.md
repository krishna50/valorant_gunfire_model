# Valorant_Gunfire_model
This repo contains the implementation of the paper


## Installation

I suggest you to install Anaconda3 in your system. 
First download Anancoda3 from https://docs.anaconda.com/anaconda/install/hashes/lin-3-64/
```bash
bash Anaconda2-2019.03-Linux-x86_64.sh
```
If you don't want to install Anaconda, we can install all the packages
from scratch using requirements.txt file
## Clone the repo
```bash
git clone https://gitlab.com/krishna50/valorant_gunfire_model.gitt
```
Install required packges using requirements.txt
```bash
sudo apt-get install sox
pip install -r requirements.txt
```


## Inference step
The following step takes a video file as input and generates a json file containing time stamps information.
 
```
python deployment.py --video_filepath $video_file_location
                            --model_path $model_path
                            --dump_dir $temporary_save_folder --save_results True 
```
The code takes $video_file_location, $model_path, and $temporary_save_folder as input. The output will be save in the same location as video
For example
```
python deployment.py --video_filepath /home/krishna/Desktop/VALORANT_test_videos/VAL_643360357.mp4 
                     --model_path ./model_checkpoint_cnn_lstm/best_check_point_24_0.92 
                     --dump_dir /mnt/newhd/sizzle_dump --save_results True 
```


Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
For any queries contact : krishna@sizzle.gg
## License
[MIT](https://choosealicense.com/licenses/mit/)